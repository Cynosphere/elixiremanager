## Elixire Manager

GET THE ELIXIRE MANAGER

ITS 10X FASTER


### License

[MIT](LICENSE.md)


### Dependencies

You can check if you have all the deps by running `elixiremanager.sh --check` (and add the `-w` flag if you're on Wayland)

- date
- jq
- libnotify
- curl

Xorg-specific dependencies:

- escrotum
- xclip

Wayland-specific dependencies:

- grim
- slurp
- wl-copy

### Getting API Key

You can get API Key from elixire web ui or you can get it by running the following command (after placing username and password):<br>
`curl -H "Content-Type: application/json" -X POST -d '{"user":"USERNAME_HERE","password":"PASSWORD_HERE"}' https://elixi.re/api/apikey`


### Installation

- Just `git clone` this script or save it. 
- Put the API Key in the relevant line.
- `chmod +x` the script if necessary


### Usage

- Run the script with `./elixireuploader.sh` (and add the `-w` flag if you're on Wayland)
- Pick an area
- Notification will appear with link, and link will be copied to the clipboard


### Binding the script to a key

Just do whatever your DE offers, most DEs allow setting custom keyboard shortcuts. Just google "$DE_NAME setting command to key".
